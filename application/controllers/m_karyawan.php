<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class m_karyawan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("m_karyawan_model");
	}	
	public function index()
	{
		$this->listm_karyawan();
	}
	 public function listm_karyawan()
	{
		$data['data_karyawan'] = $this->m_karyawan_model->tampilDataKaryawan();
		$this->load->view('karyawan', $data); 
	}
	public function inputm_Karyawan()
	{
		//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
		$data['data_karyawan'] = $this->m_karyawan_model->tampilDataKaryawan();
		if(!empty($_REQUEST)){
			$m_karyawan = $this->m_karyawan_model;
			$m_karyawan->save();
			redirect("m_karyawan/index", "refresh");
			}
		$this->load->view('input_karyawan',$data); 	
	}
	public function listdetailm_karyawan($nik)
	{
				$data['detail_karyawan'] = $this->m_karyawan_model->detail($nik);
		$this->load->view('detail_karyawan', $data); 	
	}
	public function editm_karyawan($nik)
	{
				//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['detail_karyawan'] = $this->m_karyawan_model->detail($nik);
			if(!empty($_REQUEST)){
			$m_karyawan = $this->m_karyawan_model;
			$m_karyawan->update($nik);
			redirect("m_karyawan/index", "refresh");
			}
		$this->load->view('edit_karyawan', $data); 	
	}
	public function deletekaryawan($nik)
 	{
 		$m_karyawan = $this->m_karyawan_model;
 		$m_karyawan->delete($nik);
 		redirect("m_karyawan/index", "refresh");
 	}
}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="1094" border="0">
  <tr align="center" >
    <td width="1084">
  <?php
foreach ($detail_karyawan as $data){
  $nik  =$data->nik;
  $nama =$data->nama;
  $alamat =$data->alamat;
  $telp  =$data->telp;
  $tempat_lahir  =$data->tempat_lahir;
  $tanggal_lahir =$data->tanggal_lahir;
}
  $thn_pisah = substr($tanggal_lahir, 0, 4);
  $bln_pisah = substr($tanggal_lahir, 5, 2);
  $tgl_pisah = substr($tanggal_lahir, 8, 2);
  ?>
      <form action="<?=base_url();?>m_karyawan/editm_karyawan/<?=$nik;?>" method="post" name="form1" id="form1">
      <div align="center">
        <h1>Input Data Karyawan</h1>
      </div>
      <table width="50%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#00FF66">
        <tr>
          <td width="42%">NIK</td>
          <td width="3%"> :</td>
          <td width="55%"><input type="text" name="nik" id="nik" maxlength="50" value="<?= $nik;?>"/></td>
        </tr>
        <tr>
          <td>Nama</td>
          <td>:</td>
          <td><input type="text" name="nama" id="nama" maxlength="50" value="<?= $nama;?>"/></td>
          
        </tr>
        <tr>
          <td>Alamat</td>
          <td>:</td>
          <td><textarea name="alamat" id="alamat" cols="45" rows="5" value="<?= $alamat;?>"><?= $alamat;?></textarea></td>
        </tr>
        <tr>
          <td>Telepo</td>
          <td>:</td>
          <td><input type="text" name="telp" id="telp" maxlength="50" value="<?= $telp;?>" /></td>
        </tr>
        <tr>
          <td>Tempat Lahir</td>
          <td>:</td>
          <td><input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value="<?= $tempat_lahir;?>" /></td>
        </tr>
        <tr>
          <td>Tanggal Lahir</td>
          <td>:</td>
          <td>  <select name="tgl" id="tgl" >
      <?php
        for($tgl=1;$tgl<=31;$tgl++){
          // RUMUS IF BARU 
          $select_tgl= ($tgl == $tgl_pisah) ? 'selected' : '';
      ?>
        <option value="<?=$tgl;?>" <?=$select_tgl;?>><?=$tgl;?></option>
       <?php
        }
       ?>
      </select>
      
      <select name="bln" id="bln">
      <?php
        $bulan_n = array('Januari','Februari','Maret','April',
                'Mei','Juni','Juli','Agustus','September',
                        'Oktober','November','Desember');
         for($bln=0;$bln<12;$bln++){
          $select_bln= ($bln == $bln_pisah-1) ? 'selected' : '';
      ?>
        <option value="<?=$bln+1;?>" <?=$select_bln;?>>
          <?=$bulan_n[$bln];?>
        </option>
        <?php
          }
        ?>
      </select>
      
      <select name="thn" id="thn">
      <?php
        for($thn = date('Y')-60;$thn<=date('Y');$thn++){
          $select_thn= ($thn == $thn_pisah) ? 'selected' : '';
      ?>
        <option value="<?=$thn;?>" <?=$select_thn;?>><?=$thn;?></option>
      <?php
        } 
      ?>
      </select></td>
        </tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><input type="submit" name="Submit" id="Submit" value="Simpan" />
            <input type="reset" name="reset" id="reset" value="Reset" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td><a href="<?=base_url();?>m_karyawan/listm_karyawan">
            <input type="button" name="kembali" id="kembali" value="Kembali ke Menu Sebelumnya" />
          </a></td>
        </tr>
      </table>
    </form>
    <?php  ?>
  </td>
  </tr>
</table>
</body>
</html>
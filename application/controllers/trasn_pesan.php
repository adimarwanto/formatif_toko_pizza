<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class trasn_pesan extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait (untuk manggil scrip pertama kali di jalankan )
		$this->load->model("trasn_pesan_model");
		$this->load->model("m_karyawan_model");
		$this->load->model("m_menu_model");
	}	
	public function index()
	{
		$this->listpesan();
	}
	 public function listpesan()
	{
		$data['data_karyawan'] = $this->m_karyawan_model->tampilDataKaryawan2();
		$data['data_menu'] = $this->m_menu_model->tampilDataMenu();
		$data['data_pesan'] = $this->trasn_pesan_model->tampilDataPesan2();
		$this->load->view('trans_pemesanan', $data); 
	}

	public function savedata()
	{
		$nm_pg 		= $_POST['nama'];
		$nm_pl 		= $_POST['nama_pelanggan'];
		$nm_m  		= $_POST['nama_menu'];
		$qty   		= $_POST['qty'];
		$tgl_pesan 	= date('Y-m-d');

		$cek_harga = $this->db->query('SELECT * FROM m_menu WHERE kode_menu = "'.$nm_m.'"');

			foreach($cek_harga as $row)
			{
				$harga = $row->harga;
			}
//echo $harga;die;
		$total = $qty * $harga;

		$this->db->query('INSERT INTO 
						(nik,tgl_pemesanan,nama_pelanggan,kode_menu,qty,total_harga) trasn_pesan
						values("'.$nm_pg.'","'.$tgl_pesan.'","'.$nm_pl.'",
						       "'.$nm_m.'","'.$qty.'","'.$total.'",)'
						 );
	}

	public function input_pemesanan()
	{
		$data['data_karyawan']  = $this->m_karyawan_model->tampilDataKaryawan();
		$data['data_menu'] 		= $this->m_menu_model->tampilDataMenu();
		$data['data_pesan'] 	= $this->trasn_pesan_model->tampilDataPesan2();
		if(!empty($_REQUEST)){
			$m_pesan = $this->trasn_pesan_model;
			$m_pesan->save1();
			//panggil trans terakhir
			redirect("trasn_pesan/listpesan", "refresh");
			}
		$this->load->view('input_pemesanan',$data); 	
	}

	//public function inputDetail($id_pembelian_header)
	//{
		//panggil data barang untuk kebutuhan form input
		//$data['data_barang'] = $this->barang_model->tampilDataBarang();
		//$data['id_header'] = $id_pembelian_header;
		//$data['data_pembelian_detail'] = $this->pembelian_h_model->tampilDataPembelianDetail($id_pembelian_header);

		//if (!empty($_REQUEST)) 
		//{
				# code...
			// save detail
			//$this->pembelian_h_model->savePemebelianDetail($id_pembelian_header);

			//proses update stok
			//$kd_barang =$this->input->post('kode_barang');
			//$qty =$this->input->post('qty');
			//$this->barang_model->updateStock($kd_barang, $qty);
			//redirect("pemebelian_h/inputDetail/" . $id_pembelian_header." refresh");
		//}
		//$this->load->view('input_pembelian_d',$data); 

	//}
	//public function listdetailkaryawan($nik)
	//{
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
		//$this->load->view('detail_k', $data); 	
	//}
	//public function edit($nik)
	//{
				//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				//$data['detail_karyawan'] = $this->karyawan_model->detail($nik);
			//if(!empty($_REQUEST)){
			//$m_karyawan = $this->karyawan_model;
			//$m_karyawan->update($nik);
			//redirect("karyawan/index", "refresh");
			//}
		//$this->load->view('edit_karyawan', $data); 	
	//}
}
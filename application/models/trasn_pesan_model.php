<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class trasn_pesan_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("m_menu_model");
    }

	//panggil nama table
	private $_table = "trasn_pesan";
	
	public function tampilDataPesan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataPesan2()
	{
		//menggunakan query
		//$query = $this->db->query("SELECT A. *, B.nama, C.nama_menu FROM" . $this->_table . " AS A 
			//INNER JOIN m_karyawan AS B on B.nik = A.nik
			//INNER JOIN m_menu AS C on A.kode_menu = C.kode_menu");
		$query = $this->db->query("SELECT A. *, B.nama, C.nama_menu FROM " . $this->_table . " AS A
								inner join m_karyawan as B on A.nik=B.nik
								inner join m_menu as C on A.kode_menu=C.kode_menu");

		//$query = $this->db->query("SELECT mk.nama, tp.tgl_pemesanan, tp.nama_pelanggan, tp.qty, mm.harga, tp.total_harga, mm.nama_menu from trasn_pesan as tp inner join m_menu as mm on tp.kode_menu = mm.kode_menu inner join m_karyawan as mk on tp.nik = mk.nik ");
		//$data = $query->result();

		return $query->result();
	}

	public function tampilDataPesan3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('id_pemesanan', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$qty    = $this->input->post('qty');
        $harga  = $this->input->post('harga');
        $hargamenu          = $this->m_menu_model->cariHargaMenu($kode_menu);

        $data['nik']    			= $this->input->post('nama');
        $data['tgl_pemesanan']      = date ('y-m-d');
        $data['nama_pelanggan']     = $this->input->post('nama_pelanggan');
        $data['kode_menu']         	= $this->input->post('nama_menu');
        $data['qty']           		= $qty;
        $data['total_harga']		= $qty * $hargamenu;
        $this->db->insert($this->_table, $data);
	}

	 public function save1()
    {
        $pegawai            = $this->input->post('nik');
        $nama_pelanggan     = $this->input->post('nama_pelanggan');
        $kode_menu          = $this->input->post('kode_menu');
        $qty                = $this->input->post('qty');
        //$harga                = $this->input->post('harga');

        //cari harga menu dari model menu
        $hargamenu          = $this->m_menu_model->cariHargaMenu($kode_menu);

        // fungsi untuk melihat data yang dikirim / post
        // var_dump($hargamenu); die();
        
        //$data['id_pemesanan'] = $id;
        $data['nik']            = $pegawai;
        $data['tgl_pemesanan']  = date('Y-m-d');
        $data['nama_pelanggan'] = $nama_pelanggan;
        $data['kode_menu']      = $kode_menu;
        $data['qty']            = $qty;
        // ini proses perkaliannya
        $data['total_harga']    = $qty * $hargamenu;

        $this->db->insert($this->_table, $data);
    }
	public function update($id_pemesanan)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['nik']					= $this->input->post('nik');
		$data['tgl_pemesanan']			= date ('y-m-d');
		$data['nama_pelanggan']			= $this->input->post('nama_pelanggan');
		$data['kode_menu']				= $this->input->post('kode_menu');
		$data['qty']					= $this->input->post('qty');
		$data['total_harga']			= $this->input->post('total_harga');

		$this->db->where('id_pemesanan', $id_pemesanan);
		$this->db->update($this->_table, $data);
	}
	public function detail($id_pemesanan)
	{
		$this->db->select('*');
		$this->db->where('id_pemesanan', $id_pemesanan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function detail_edit($id_pemesanan)
	{

		$this->db->select('*');
		$this->db->where('id_pemesanan', $id_pemesanan);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	
	
	
}
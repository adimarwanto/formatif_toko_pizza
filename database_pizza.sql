-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 24, 2019 at 02:12 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_pizza`
--
CREATE DATABASE IF NOT EXISTS `toko_pizza` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_pizza`;

-- --------------------------------------------------------

--
-- Table structure for table `m_karyawan`
--

CREATE TABLE `m_karyawan` (
  `nik` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_karyawan`
--

INSERT INTO `m_karyawan` (`nik`, `nama`, `alamat`, `telp`, `tempat_lahir`, `tanggal_lahir`, `flag`) VALUES
('19021', 'Yudhi Slamet', 'Jl.Kebon Baru Utara No 7', '02135466374', 'jakarta', '1995-01-17', 1),
('19022', 'Bagus Riyadi', 'Jl.Kampung Sawah No 9 ', '0218838383', 'jakarta', '1989-04-19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_menu`
--

CREATE TABLE `m_menu` (
  `kode_menu` varchar(5) NOT NULL,
  `nama_menu` varchar(30) NOT NULL,
  `harga` float NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_menu`
--

INSERT INTO `m_menu` (`kode_menu`, `nama_menu`, `harga`, `keterangan`, `flag`) VALUES
('PZ001', 'Veggie Garden Pizza', 76000, 'Jagung, Jamur, Keju Mozarella, Paprika Merah, ', 1),
('PZ002', 'Tuna Melt Pizza', 80000, 'Irirsan Daging Ikan Tuna, Butiran Jagung, Saos Mayones', 1),
('PZ003', 'Cheeseburger Pizza', 79000, 'Daging Sapi Cincang, Daging Sapi dan Daging ayam Keju', 1);

-- --------------------------------------------------------

--
-- Table structure for table `trasn_pesan`
--

CREATE TABLE `trasn_pesan` (
  `id_pemesanan` int(11) NOT NULL,
  `nik` varchar(5) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `kode_menu` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trasn_pesan`
--

INSERT INTO `trasn_pesan` (`id_pemesanan`, `nik`, `tgl_pemesanan`, `nama_pelanggan`, `kode_menu`, `qty`, `total_harga`) VALUES
(21, '19021', '2019-02-24', 'andi', 'PZ001', 4, 304000),
(22, '19021', '2019-02-24', 'andi', 'PZ003', 3, 237000),
(23, '19021', '2019-02-24', 'andi', 'PZ003', 3, 237000),
(24, '19021', '2019-02-24', 'Siti', 'PZ001', 4, 304000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_karyawan`
--
ALTER TABLE `m_karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `m_menu`
--
ALTER TABLE `m_menu`
  ADD PRIMARY KEY (`kode_menu`);

--
-- Indexes for table `trasn_pesan`
--
ALTER TABLE `trasn_pesan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `trasn_pesan`
--
ALTER TABLE `trasn_pesan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
defined('BASEPATH') OR Exit('No direct script access allowed');

class m_menu extends CI_Controller
{
	Public function __construct()
	{
		parent::__construct();
		//load model terkait
		$this->load->model("m_menu_model");
	}	
	public function index()
	{
		$this->listmenu();
	}
	public function listmenu()
	{
		$data['data_menu'] = $this->m_menu_model->tampilDataMenu();
		//$data['data_barang'] = $this->barang_model->tampilDataBarang2();
		$this->load->view('menu', $data); 	 
	}
	public function inputMenu()
	{
		//$data['data_jenis'] = $this->jenis_model->tampilDataJenis();
		$data['data_menu'] = $this->m_menu_model->tampilDataMenu();
		if(!empty($_REQUEST)){
			$m_menu = $this->m_menu_model;
			$m_menu->save();
			redirect("m_menu/index", "refresh");
			}
		$this->load->view('input_menu',$data); 	
	}
	public function listdetailmenu($kode_menu)
	{
				$data['detail_menu'] = $this->m_menu_model->detail($kode_menu);
		$this->load->view('detail_menu', $data); 	
	}	
	public function editmenu($kode_menu)
	{
				//$data['data_jabatan'] = $this->jabatan_model->tampilDataJabatan();
				$data['detail_menu'] = $this->m_menu_model->detail($kode_menu);
			if(!empty($_REQUEST)){
			$m_menu = $this->m_menu_model;
			$m_menu->updatemenu($kode_menu);
			redirect("m_menu/index", "refresh");
			}
		$this->load->view('edit_menu', $data); 	
	}
	public function deletemenu($kode_menu)
 	{
 		$m_menu = $this->m_menu_model;
 		$m_menu->delete($kode_menu);
 		redirect("m_menu/index", "refresh");
 	}

		
}
	
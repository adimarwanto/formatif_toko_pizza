<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class m_karyawan_model extends CI_Model
{
	//panggil nama table
	private $_table = "m_karyawan";
	
	public function tampilDataKaryawan()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
		//$this->db->query('SELECT * FROM m_karyawan');
	}

	public function tampilDataKaryawan2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT mk.nama FROM m_karyawan as mk inner join trasn_pesan as tp on tp.nik = mk.nik ");
		return $query->result();
	}

	public function tampilDataKaryawan3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['nik']				= $this->input->post('nik');
		$data['nama']				= $this->input->post('nama');
		$data['alamat']				= $this->input->post('alamat');
		$data['telp']				= $this->input->post('telp');
		$data['tempat_lahir']		= $this->input->post('tempat_lahir');
		$data['tanggal_lahir']		= $tgl_gabungan;
		$data['flag']				= 1;
		$this->db->insert($this->_table, $data);
	}
	public function update($nik)
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['nik']				= $this->input->post('nik');
		$data['nama']				= $this->input->post('nama');
		$data['alamat']				= $this->input->post('alamat');
		$data['telp']				= $this->input->post('telp');
		$data['tempat_lahir']		= $this->input->post('tempat_lahir');
		$data['tanggal_lahir']		= $tgl_gabungan;
		$data['flag']				= 1;

		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function detail_edit($nik)
	{

		$this->db->select('*');
		$this->db->where('nik', $nik);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
	}

	
	
	
}
<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class m_menu_model extends CI_Model
{
	//panggil nama table
	private $_table = "m_menu";
	
	public function tampilDataMenu()
	{
		// seperti : select * from <nama_table>
		return $this->db->get($this->_table)->result();
	}

	public function tampilDataMenu2()
	{
		//menggunakan query
		$query = $this->db->query("SELECT * FROM m_menu WHERE kode_menu = ");
		return $query->result();
	}

	public function tampilDataMenu3()
	{
		//menggunakan query 
		$this->db->select('*');
		$this->db->order_by('kode_menu', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	public function save()
	{
		$tgl = $this->input->post('tgl');
		$bln = $this->input->post('bln');
		$thn = $this->input->post('thn');
		$tgl_gabungan = $thn."-".$bln."-".$tgl;
		
		$data['kode_menu']			= $this->input->post('kode_menu');
		$data['nama_menu']			= $this->input->post('nama_menu');
		$data['harga']				= $this->input->post('harga');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']					= 1;
		$this->db->insert($this->_table, $data);
	}
	  
	public function updatemenu($kode_menu)
	{		
		$data['kode_menu']			= $this->input->post('kode_menu');
		$data['nama_menu']			= $this->input->post('nama_menu');
		$data['harga']				= $this->input->post('harga');
		$data['keterangan']		= $this->input->post('keterangan');
		$data['flag']					= 1;
		$this->db->where('kode_menu', $kode_menu);
		$this->db->update($this->_table, $data);
	}
	public function detail($kode_menu)
	{
		$this->db->select('*');
		$this->db->where('kode_menu', $kode_menu);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	  public function cariHargaMenu($kode_menu)
    {
        $query	= $this->db->query("SELECT * FROM " . $this->_table . " WHERE flag = 1 AND kode_menu = '$kode_menu'");
        $hasil = $query->result();	

        foreach($hasil as $data) {
            $harganya = $data->harga;
        }

        //yang dikirim hasil harga
        return $harganya;
    }
    public function delete($kode_menu)
	{
		$this->db->where('kode_menu', $kode_menu);
		$this->db->delete($this->_table);
	}


	public function editdetail($kode_menu)
	{
		$this->db->select('*');
		$this->db->where('kode_menu', $kode_menu);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function updateStok($kode_barang, $qty)
    {
        //panggil data detail stok
        $cari_stok = $this->detail($kode_barang);
        foreach ($cari_stok as $data) {
            $stok = $data->stock;
        }
        
        //proses update stok table barang
        $jumlah_stok     = $stok + $qty;
        $data_barang['stock']    = $jumlah_stok;
        
        $this->db->where('kode_barang', $kode_barang);
        $this->db->update('barang', $data_barang);
    }


	
	
	
}